// click del bottone torna alla home
$("#back-home").click(function(e) {
  var url = './'
   $(location).attr('href', url);
  e.preventDefault()
});

//click del bottone submit della recensione
$("#submit_real_time").click(function() {
  $('#val_stimato').html('-')
  if(!$.trim($('#real_time_rec').val())){
    $('#real_time_rec').val('');
    $('#problem_alert').fadeIn(500).fadeOut(1500);
  }else{
    $(this).addClass('hidden');
    $('#spinner_div').removeClass('hidden');
    setTimeout(function(){
      $('#spinner_message').html('Ci siamo quasi...');
      setTimeout(function(){
        $('#spinner_message').html("Ancora qualche secondo di pazienza...");
      }, 10000)
    }, 10000);
    $.ajax({
      type : 'POST',
      url : 'api/inline',
      data : $('#form_rec_realt').serialize(),
      success : function(data, textStatus, request) {
        // alert(request.getResponseHeader('Location'));
        var response = request.getResponseHeader('Location').split('/');
        var numero = response[response.length - 1];
        var number = Math.floor((Math.random() * 4) + 1);
        $('#song').attr('src', 'assets/' + number + '.mp3');
        $('#song').attr('autoplay', 'true');
        $.getJSON('api/inline/waiting/' + numero).done(function(data) {
          $('#spinner_div').addClass('hidden');
          $('#submit_real_time').removeClass('hidden');
          $('#val_stimato').html(data.overall_algo);
          $('audio').each(function() {
            this.pause(); // Stop
            // playing
          });
        });
      },
      error : function(request, textStatus, errorThrown) {
        $('#val_stimato').html(textStatus);
      }
    });
  }

});

$( document ).ready(function() {
    $('#search_by_name').val(''); //pulizia serachbar
		$('#testo_recensione').html(''); //pulizia textarea
    google.charts.load('current', {'packages':['corechart']});
    // caricamento nomi hotel
        var allHotels = [];
        $.getJSON("./api/hotels/").done(function(data) {
            $(data).each(function( index ) {
              allHotels.push(this.name);
            });
            $( "#search_by_name" ).autocomplete({
                source: allHotels
            });
        });
})
/******************************************************************************
*                               EVENTI                                        *
******************************************************************************/
//pressione di INVIO nella searchbar
$('#search_by_name').keypress(function(e) {
  if (e.which == 13) {// Enter key pressed
    var name = $("#search_by_name").val();
    if(!$.trim(name)){
      $('#problem_alert').fadeIn(500).fadeOut(1500);
      $("#search_by_name").val('');
      $('#not_found_div').addClass('hidden');
      $('#page_content').addClass('hidden');
    }else{
      $("#gmap").attr('src', '');
      $('#testo_recensione').html(''); //pulizia textarea
      $('#sentiment_rec_rand').text('-'); //rating recensione random
      $('#sentiment_alg_rand').text('-'); //rating algoritmo random
      find_hotel_by_name(name);
    }
      e.preventDefault();
    }
  })
// click del tasto cerca della searchbar
$('#search_by_name_button').click(function(e) {
    var name = $("#search_by_name").val();
    if(!$.trim(name)){
      $('#problem_alert').fadeIn(500).fadeOut(1500);
      $("#search_by_name").val('');
      $('#not_found_div').addClass('hidden');
      $('#page_content').addClass('hidden');
    }else{
    $("#gmap").attr('src', '');
		$('#testo_recensione').html(''); //pulizia textarea
		$('#sentiment_rec_rand').text('-'); //rating recensione random
		$('#sentiment_alg_rand').text('-'); //rating algoritmo random
	  find_hotel_by_name(name);
    }
	  e.preventDefault();
})
// click del bottone per ricerca hotel random
$('#random-hotel-select').click(function(e) {
    $("#gmap").attr('src', '');
		$('#testo_recensione').html(''); //pulizia textarea
		$('#sentiment_rec_rand').text('-'); //rating recensione random
		$('#sentiment_alg_rand').text('-'); //rating algoritmo random
		$('#search_by_name').val('');
	  find_random_hotel();
	  e.preventDefault();
})
// click del bottone cerca per trovare una recensione dell'hotel a caso
$('#search_review').click(function(e){
	  getRecensioneRandom();
	  e.preventDefault()
})

// click del bottone live demo
$('#live-demo').click(function(e){
    var url = './inline_eval.jsp'
	   $(location).attr('href', url);
	  e.preventDefault()
})
/******************************************************************************
*                               FUNZIONI                                      *
******************************************************************************/
/**
* Funzione che gestisce il caricamento di una recensione random
* @API richiamate: reviews/positive
*                  reviews/neutral
*                  reviews/negative
*                  reviews/neutral?algoritmo=1
*                  reviews/negative?algoritmo=1
*                  reviews/algoritmo=1
**/
function getRecensioneRandom(){
  var id = $('#search_review').attr('data-hotel-index');
  var radio = $('#r_sent_algo').prop("checked"); //checked = rec, unchecked= alg
  var sentiment;
  var request = "./api/hotels/"+id+"/reviews/";
  var reviewObj = $.Deferred();
  $.when(reviewObj).done(function (recensione) {
    $('#testo_recensione').html(recensione.content);
    $('#sentiment_alg_rand').text(recensione.overall_algoritmo);
    $('#sentiment_rec_rand').text(recensione.overall_review);
  });
  if($('#r_positivo').prop("checked")){
    if(radio){
      //CERCA POSITIVA RANDOM DA ALGORITMO
      sentiment = "positiva";
      request += "positive?algoritmo=1";
    }else{
      //CERCA POSITIVA RANDOM DA OVERALL
      sentiment = "positiva";
      request += "positive";
    }
  }else if($('#r_neutro').prop("checked")){
    if(radio){
      //CERCA NEUTRO RANDOM DA ALGORITMO
      sentiment = "neutro";
      request += "neutral?algoritmo=1";
    }else{
      //CERCA NEUTRO RANDOM DA OVERALL
      sentiment = "neutro";
      request += "neutral";
    }
  }else if ($('#r_negativo').prop("checked")){
    if(radio){
      //CERCA NEGATIVA RANDOM DA ALGORITMO
      sentiment = "negativa";
      request += "negative?algoritmo=1";
    }else{
      //CERCA NEGATIVA RANDOM DA OVERALL
      sentiment = "negativa";
      request += "negative";
    }
  }
  $.getJSON(request).done(function(data) {
      var random = Math.floor((Math.random() * data.length));
      reviewObj.resolve(data[random]);
  }).error(function(error){
    var data = [];
    if(error.status == 404){
      data["content"] = "Nessuna recensione "+sentiment+" presente"
      data["overall_algoritmo"] = "-"
      data["overall_review"] = "-"
      reviewObj.resolve(data);
    }else{
      data["content"] = "-"
      data["content"] = "Errore: "+ error.statusText;
      data["overall_algoritmo"] = "-"
      data["overall_review"] = "-"
      reviewObj.resolve(data);
    }
  });
}
/**
* Funzione per impostare la mappa di google
* @Param: name: nome dell'hotel
*         place: luogo dell'hotel
*         zoom: zoom con il quale si vuole impostare la mappa
**/
function setGMap(name, place, zoom){
  var src = "https://www.google.com/maps/embed/v1/place?q="
  var nomerplc = name.replace(/ /g,"%20");
  var placerplc = place.replace(/ /g,"%20");
  src += nomerplc+"%20"+placerplc+"zoom="+zoom+"&key=AIzaSyDNtZmpyG4fo0iUe8z8Ob13AT4-96hIRSI"
  $("#gmap").attr('src', src);
}
/**
* Funzione per il calcolo delle tabelle
* @Param: id: id dell'hotel
*         timeDatasRev: dati media temporale recensioni
*         timeDatasAlg: dati media temporale algoritmo
* @Funzioni esterne richiamate: disegnaGrafico()
* @API richiamate: reviews/positive
*                  reviews/neutral
*                  reviews/negative
                   reviews
*                  reviews/positive?algoritmo=1
*                  reviews/neutral?algoritmo=1
*                  reviews/negative?algoritmo=1
*                  reviews/algoritmo=1
**/
function statistiche(id, timeDatasRev, timeDatasAlg){
  var positiveOverall = $.Deferred();
  var neutralOverall = $.Deferred();
  var negativeOverall = $.Deferred();
  var totOverall = $.Deferred();
  var positiveAlgoritmo = $.Deferred();
  var neutralAlgoritmo = $.Deferred();
  var negativeAlgoritmo = $.Deferred();
  var totAlgoritmo = $.Deferred();
	var timelineDatas = [['Year', 'Overall Recensione']];

	$(timeDatasRev.entry).each(function(index, data) {
		timelineDatas.push([String(data.key), data.value]);
	});
	$(timelineDatas).each(function(index, data) {
    if(index != 0){
      timelineDatas[index].push(timeDatasAlg.entry[index-1].value);
    }else{
      timelineDatas[index].push('Stima Algoritmo');
    }
	});

  $.when(positiveAlgoritmo, neutralAlgoritmo, negativeAlgoritmo, totAlgoritmo)
  .done(function(posa, neua, nega, tota){
    var datiAlgoritmo = [];

    //imposta valori dati algoritmo
    $("#positive_sentiment_algoritmo").html(posa);
    $("#neutre_sentiment_algoritmo").html(neua);
    $("#negative_sentiment_algoritmo").html(nega);
    $("#tot_algoritmo").html(tota);

    //algoritmo
    datiAlgoritmo.push(['Task', "\'"+tota+"\'" ]);
    datiAlgoritmo.push(['Positive', posa]);
    datiAlgoritmo.push(['Neutre', neua]);
    datiAlgoritmo.push(['Negative', nega]);

    disegnaGrafico("piechart_algoritmo", datiAlgoritmo, "pie");
  });

  $.when( positiveOverall, neutralOverall, negativeOverall, totOverall)
  .done(function ( poso, neuo, nego, toto) {
    var datiRecensione = [];

    //imposta valori dati recensione
    $("#positive_sentiment_recensione").html(poso);
    $("#neutre_sentiment_recensione").html(neuo);
    $("#negative_sentiment_recensione").html(nego);
    $("#tot_recensione").html(toto);

    //recensione
    datiRecensione.push(['Task', "\'"+toto+"\'" ]);
    datiRecensione.push(['Positive', poso]);
    datiRecensione.push(['Neutre', neuo]);
    datiRecensione.push(['Negative', nego]);

    disegnaGrafico("piechart_recensione", datiRecensione, "pie");
  });
  disegnaGrafico("curvechart", timelineDatas, "line");
//recensioni Overall
  $.getJSON("./api/hotels/"+id+"/reviews/positive").done(function(data) {
      positiveOverall.resolve(data.length);
  }).error(function(error){
		if(error.status == 404){
			positiveOverall.resolve(0);
		}
	});
  $.getJSON("./api/hotels/"+id+"/reviews/neutral").done(function(data) {
      neutralOverall.resolve(data.length);
  }).error(function(error){
		if(error.status == 404){
			neutralOverall.resolve(0);
		}
	});
  $.getJSON("./api/hotels/"+id+"/reviews/negative").done(function(data) {
      negativeOverall.resolve(data.length);
  }).error(function(error){
		if(error.status == 404){
			negativeOverall.resolve(0);
		}
	});
  $.getJSON("./api/hotels/"+id+"/reviews").done(function(data) {
      totOverall.resolve(data.length);
  }).error(function(error){
		if(error.status == 404){
			totOverall.resolve(0);
		}
	});
  //recensioni algoritmo
  $.getJSON("./api/hotels/"+id+"/reviews/positive?algoritmo=1").done(function(data) {
      positiveAlgoritmo.resolve(data.length);
  }).error(function(error){
		if(error.status == 404){
			positiveAlgoritmo.resolve(0);
		}
	});
  $.getJSON("./api/hotels/"+id+"/reviews/neutral?algoritmo=1").done(function(data) {
      neutralAlgoritmo.resolve(data.length);
  }).error(function(error){
		if(error.status == 404){
			neutralAlgoritmo.resolve(0);
		}
	});
  $.getJSON("./api/hotels/"+id+"/reviews/negative?algoritmo=1").done(function(data) {
      negativeAlgoritmo.resolve(data.length);
  }).error(function(error){
		if(error.status == 404){
			negativeAlgoritmo.resolve(0);
		}
	});
  $.getJSON("./api/hotels/"+id+"/reviews?algoritmo=1").done(function(data) {
      totAlgoritmo.resolve(data.length);
  }).error(function(error){
		if(error.status == 404){
			totAlgoritmo.resolve(0);
		}
	});
}
/**
* Funzione per disegnare un grafico generico
* @Param: idGrafica: l'id html del grafico da disegnare
*         dati: i dati da rappresentare
*         tipo: il tipo di grafico (pie, chart)
**/
function disegnaGrafico(idGrafico, dati, tipo){
    google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable(dati);
    if(tipo == "pie"){
      var options = {
				slices: {0: {color: 'green'}, 1: {color: 'blue'}, 2: {color: 'red'}},
        title: 'Recensioni',
        is3D: true,
        width: 550,
        height: 200,
        chartArea:{left:20,top:0,width:'100%',height:'100%'}
      };
      var chart = new google.visualization.PieChart(document.getElementById(idGrafico));
    }else if(tipo =="line"){
      var options = {
				vAxis : {
					minValue: 0,
					maxValue: 5,
					},
        title: 'Confronto su timeline',
        curveType: 'function',
        width: 1100,
        height:200,
        legend: 'none'
      };
      var chart = new google.visualization.LineChart(document.getElementById(idGrafico));
    }
    chart.draw(data, options);
  }
}
/**
* Funzione per il recupero dei dati di un hotel dato il nome
* @Param: name: nome dell'hotel
* @Funzioni esterne richiamate: statistiche()
*                               setGMap()
* @API richiamate: hotels/{nome_hotel}
**/
function find_hotel_by_name(name) {
	var hotel_name = $.Deferred();
	var location = $.Deferred();
	var hotel_index = $.Deferred();
	var timeDatasAlg = $.Deferred();
	var timeDatasRev = $.Deferred();
	var showPage = $.Deferred();
  var accuracy = $.Deferred();

	$.when(showPage).done(function(showPage){
		if(showPage){
      $('#not_found_div').addClass('hidden');
			$('#page_content').removeClass('hidden');
		}else{
      $('#page_content').addClass('hidden');
			$('#not_found_div').removeClass('hidden');

		}
	});
	$.when(hotel_name, location, hotel_index, timeDatasRev, timeDatasAlg, accuracy).done(function (hotel_name, location, hotel_index, timeDatasRev, timeDatasAlg, accuracy){
		statistiche(hotel_index, timeDatasRev, timeDatasAlg);
		setGMap(hotel_name, location, 17);
		$("#hotel-name").text(hotel_name);
		$("#posizione").text(location);
		$('#search_review').attr('data-hotel-index', hotel_index);
    $('#accuratezza').html(' '+accuracy.toFixed(2)+'%');
    if(accuracy <= 33.33){
      $('#accuracy_div').removeClass('alert-info').removeClass('alert-success').addClass('alert-danger');
    }else if(accuracy <= 66.66){
      $('#accuracy_div').removeClass('alert-danger').removeClass('alert-success').addClass('alert-info');
    }else{
      $('#accuracy_div').removeClass('alert-danger').removeClass('alert-info').addClass('alert-success');
    }
	});
  $.getJSON("./api/hotels/" + name).done(function(data) {
    accuracy.resolve(data.correttezza);
    hotel_name.resolve(data.name);
    location.resolve(data.place);
    hotel_index.resolve(data.index);
		timeDatasAlg.resolve(data.andamento_algoritmo);
		timeDatasRev.resolve(data.andamento_review);
		showPage.resolve(true);
  }).error(function(error) {
    if(error.status == 404){
			showPage.resolve(false);
		}else{
			console.log(error.statusText);
			showPage.resolve(false);
		}
  });
}
/**
* Funzione per il recupero dei dati di un hotel a casp
* @Funzioni esterne richiamate: statistiche()
*                               setGMap()
* @API richiamate: hotels/{nome_hotel}
**/
function find_random_hotel() {
  var hotels_number;
  var hotel_index = $.Deferred();
	var timeDatasAlg = $.Deferred();
	var timeDatasRev = $.Deferred();
	var hotel_name = $.Deferred();
	var location = $.Deferred();
  var accuracy = $.Deferred();

	$.when(hotel_index, hotel_name, location, timeDatasRev, timeDatasAlg, accuracy).done(function (hotel_index, hotel_name, location, timeDatasRev, timeDatasAlg, accuracy){
		statistiche(hotel_index, timeDatasRev, timeDatasAlg);
		setGMap(hotel_name, location, 17);
		$("#hotel-name").text(hotel_name);
		$("#posizione").text(location);
		$('#search_review').attr('data-hotel-index', hotel_index);
    $('#accuratezza').html(' '+accuracy.toFixed(2)+'%');
    if(accuracy <= 33.33){
      $('#accuracy_div').removeClass('alert-info').removeClass('alert-success').addClass('alert-danger');
    }else if(accuracy <= 66.66){
      $('#accuracy_div').removeClass('alert-danger').removeClass('alert-success').addClass('alert-info');
    }else{
      $('#accuracy_div').removeClass('alert-danger').removeClass('alert-info').addClass('alert-success');
    }
	});

    $.getJSON("./api/hotels").done(function(data) {
        hotels_number = data.length;
        number = Math.floor((Math.random() * hotels_number) + 1);
        $.getJSON("./api/hotels/random/" + number).done(function(data) {
            accuracy.resolve(data.correttezza);
            hotel_name.resolve(data.name);
            location.resolve(data.place);
						timeDatasAlg.resolve(data.andamento_algoritmo);
						timeDatasRev.resolve(data.andamento_review);
      			hotel_index.resolve(data.index);
        }).error(function(data) {
            console.log(data);
        });
    });
	$('#not_found_div').addClass('hidden');
  $('#page_content').removeClass('hidden');
}

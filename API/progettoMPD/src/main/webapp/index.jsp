<html>
<head>
	<script src="./assets/jquery-2.2.4.js"></script>
	<link rel="stylesheet" href="./assets/bootstrap-3.3.6-dist/css/bootstrap.min.css" />
	<link rel="stylesheet" href="./assets/font-awesome-4.6.3/css/font-awesome.min.css" />
	<link rel="stylesheet" href="./assets/icomoon/style.css">
	<link rel="stylesheet" href="./assets/css/ace-fonts.css" />
	<link rel="stylesheet" href="./assets/css/ace.css" />
	<link rel="stylesheet" href="./assets/jquery-autocomplete/jquery-ui.css">
	<link rel="stylesheet" href="./assets/css/personal.css" />
  <script src="./assets/jquery-autocomplete/jquery-ui.js"></script>
	<script src="./assets/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>
<body class="no-skin">
	<div class="container">
		<div class="row row-centered">
			<div class="col-xs-12 text-center" >
				<h1>HOTEL REVIEW</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-4">
			</div>
			<div class="col-xs-4 text-center">
				<div class="input-group">
					<input type="text" class="form-control search-query text-center" id="search_by_name" placeholder="Cerca un hotel">
					<span class="input-group-btn">
						<button type="button" class="btn btn-purple btn-sm" id="search_by_name_button">
							Cerca
						</button>
					</span>
				</div>
			</div>
			<div class="col-xs-4 text-center">
				<span class="alert alert-danger text-center collapse" id="problem_alert">Inserisci il nome di un hotel</span>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-3">
			</div>
			<div class="col-xs-6 text-center">
				<button class="btn btn-md btn-warning" id="random-hotel-select">
					<span class="icon-shuffle"></span>
					<span class="bigger-110 no-text-shadow">SELEZIONA UN HOTEL RANDOM</span>
				</button>
				<button class="btn btn-md btn-success" id="live-demo">
					<span class="ace-icon fa fa-eye"></span>
					<span class="bigger-110 no-text-shadow">LIVE DEMO</span>
				</button>
			</div>
			<div class="col-xs-3">
			</div>
		</div>
		<br>
		<hr>
		<div class="row">
			<div class="col-xs-12 text-center hidden" id="not_found_div">
				<h4 class="red">HOTEL NON TROVATO</h4>
				<iframe id="notfound_gif" src=".\assets\image\4042.gif" class="giphy-embed"></iframe>
			</div>
		</div>
		<div class="page-content hidden" id="page_content">
			<div class="row">
				<div class="col-xs-6">
					<div class="row">
						<div class="col-xs-12">
							<div class="alert alert-info col-xs-3 text-center">
								<strong>HOTEL</strong>
							</div>
							<div id ="hotel-name" class="col-xs-9 alert">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div  class="alert alert-info col-xs-3 text-center">
								<strong>POSIZIONE</strong>
							</div>
							<div id ="posizione" class="col-xs-9 alert">
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6">
					<iframe class="gmap" id="gmap">
					</iframe>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 text-center">
					<hr>
					<%--  valuta sostituzione con etichetta--%>
					<h3 class="text-primary"><b>STATISTICHE RECENSIONI POSITIVE/NEUTRE/NEGATIVE</b></h3>
					<hr>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-3">
				</div>
				<div class="col-xs-6">
					<div id="accuracy_div" class="alert alert-info text-center">
						Accuratezza di calcolo stimata per questo hotel:
						<strong><span id="accuratezza"></span></strong>
					</div>
				</div>
				<div class="col-xs-3">
				</div>
			</div>

			<div class="row">
				<div class="col-xs-6">
					<h4 class="text-center text-success"><b>OVERALL</b></h4>
				</div>
				<div class="col-xs-6">
					<h4 class="text-center text-success"><b>ALGORITMO</b></h4>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
						<table id="dynamic-table" class="table table-striped table-bordered table-hover dataTable no-footer" role="grid" aria-describedby="dynamic-table_info">
							<thead>
							</thead>
							<tbody>
								<tr role="row" class="odd">
									<td>
										NUMERO RECENSIONI <span class="green"><strong>POSITIVE</strong></span>
									</td>
									<td id="positive_sentiment_recensione">
									</td>
								</tr>
								<tr role="row" class="even">
									<td>
										NUMERO RECENSIONI <span class="blue"><strong>NEUTRE</strong></span>
									</td>
									<td id="neutre_sentiment_recensione">
									</td>
								</tr>
								<tr role="row" class="odd">
									<td>
										NUMERO RECENSIONI <span class="red"> <strong>NEGATIVE</strong></span>
									</td>
									<td id="negative_sentiment_recensione">
									</td>
								</tr>
								<tr role="row" class="even">
									<td>
										<strong>TOTALE</strong>
									</td>
									<td id="tot_recensione">
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-xs-6">
					<div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
						<table id="dynamic-table" class="table table-striped table-bordered table-hover dataTable no-footer" role="grid" aria-describedby="dynamic-table_info">
							<thead>
							</thead>
							<tbody>
								<tr role="row" class="odd">
									<td>
										NUMERO RECENSIONI <span class="green"><strong>POSITIVE</strong></span>
									</td>
									<td id="positive_sentiment_algoritmo">
									</td>
								</tr>
								<tr role="row" class="even">
									<td>
										NUMERO RECENSIONI <span class="blue"><strong>NEUTRE</strong></span>
									</td>
									<td id="neutre_sentiment_algoritmo">
									</td>
								</tr>
								<tr role="row" class="odd">
									<td>
										NUMERO RECENSIONI <span class="red"> <strong>NEGATIVE</strong></span>
									</td>
									<td id="negative_sentiment_algoritmo">
									</td>
								</tr>
								<tr role="row" class="even">
									<td>
										<strong>TOTALE</strong>
									</td>
									<td id="tot_algoritmo">
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

			</div>


			<div class="row">
				<div class="col-xs-6">
					<%--  valuta sostituzione con etichetta--%>
						<h4 class="text-center text-success"><b>VALORI OVERALL</b></h4>
				</div>
				<div class="col-xs-6">
					<%--  valuta sostituzione con etichetta--%>
					<h4 class="text-center text-success"><b>VALORI STIMATI</b></h4>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<div id="piechart_recensione"></div>
				</div>
				<div class="col-xs-6">
					<div id="piechart_algoritmo"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="row">
						<div class="col-xs-12">
							<h4 class="text-center text-success"><b>CONFRONTO VALORI MEDI ANNUALI</b></h4>
							<div id="curvechart"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 text-center">
							<div class="row">
								<div class="col-xs-12 text-center">
									<h5 class="green">LEGENDA</h5>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 text-center">
									<span class="blue"><strong>_____</strong></span> STIMA ALGORITMO&nbsp;&nbsp;
									<span class="red"><strong>_____</strong></span> OVERALL RECENSIONE
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row text-center">
				<div class="col-xs-12">
					<hr>
					<h3 class="text-primary"><b>LEGGI UNA RECENSIONE RANDOM!</b></h3>
					<hr>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-4">
					<div class="widget-box">
						<div class="widget-header text-center">
							<h4>CERCA RECENSIONE CON</h4>
						</div>
						<div class="widget-body">
							<div class="widget-main">
								<div class="row">
									<div class="col-xs-5 text-center">
										<small class="muted smaller-90">Overall</small>
									</div>
									<div class="col-xs-2">
										<label>
											<input id="r_sent_algo" name="switch-field-1" class="ace ace-switch" type="checkbox">
											<span class="lbl" data-lbl="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"></span>
										</label>
									</div>
									<div class="col-xs-5 text-center">
										<small class="muted smaller-90">Algoritmo</small>
									</div>
								</div>

								<div class="row">
									<div class="col-xs-12 text-center">
										<div class="btn-group" data-toggle="buttons">
											<label class="btn btn-primary active">
												<input type="radio" name="options" id="r_positivo" autocomplete="off" checked> Positivo
											</label>
											<label class="btn btn-primary">
												<input type="radio" name="options" id="r_neutro" autocomplete="off"> Neutro
											</label>
											<label class="btn btn-primary">
												<input type="radio" name="options" id="r_negativo" autocomplete="off"> Negativo
											</label>
										</div>
									</div>
								</div>
								<br>
								<div class="row">
										<div class="col-xs-12 text-center">
											<button id="search_review" data-hotel-index="" class="btn btn-white btn-info btn-bold">
												<i class="ace-icon glyphicon glyphicon-search"></i>
												Cerca
											</button>
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-4">
					<div class="widget-box">
						<div class="widget-header text-center">
							<h4 class="smaller">TESTO RECENSIONE</h4>
						</div>
						<div class="widget-body">
							<div class="widget-main" id="testo_recensione">
							</div>
						</div>
					</div>
					<%-- <textarea class="form-control" id="testo_recensione" placeholder="Testo recensione" readonly></textarea> --%>
				</div>
				<div class="col-xs-4">
					<div class="row">
						<div class="col-xs-12 text-center">
							<div class="widget-box">
								<div class="widget-header">
									<h4 class="smaller">VALORE RECENSIONE</h4>
								</div>
								<div class="widget-body">
									<div class="widget-main">
										<strong id="sentiment_rec_rand">-</strong>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 text-center">
							<div class="widget-box">
								<div class="widget-header">
									<h4 class="smaller">VALORE ALGORITMO</h4>
								</div>
								<div class="widget-body">
									<div class="widget-main">
										<strong id="sentiment_alg_rand">-</strong>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<script src="./assets/scripts/interface.js"></script>
</body>
</html>

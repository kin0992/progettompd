<html>
<head>
	<script src="./assets/jquery-2.2.4.js"></script>
	<script src="./assets/jquery-2.2.4.js"></script>
	<link rel="stylesheet" href="./assets/bootstrap-3.3.6-dist/css/bootstrap.min.css" />
	<link rel="stylesheet" href="./assets/font-awesome-4.6.3/css/font-awesome.min.css" />
	<link rel="stylesheet" href="./assets/css/ace-fonts.css" />
	<link rel="stylesheet" href="./assets/css/ace.css" />
	<link rel="stylesheet" href="./assets/css/live.css" />
	<script src="./assets/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
</head>
<body class="no-skin">
	<div class="container">
		<div class="page-content vertical-center">
			<div class="row">
				<div class="col-xs-12 text-center">
					<h1 class="blue">PROVA REAL TIME!</h1>
					<div class="col-xs-3">
						<button type="button" id="back-home" class="btn btn-lg btn-white btn-primary">
							<i class="ace-icon fa fa-arrow-left icon-on-left"></i>
							Torna alla home
						</button>
						<br>
						<br>
						<div class="alert alert-danger text-center collapse col-xs-12" id="problem_alert">
							Inserire una recensione valida
							<br>
						</div>
					</div>
					<div class="widget-box col-xs-6">
						<div class="widget-header">
							<h4 class="widget-title">RECENSIONE</h4>
						</div>

						<div class="widget-body" style="display: block;">
							<div class="widget-main">
								<div>
									<form action="api/inline" method="post" id="form_rec_realt">
										<textarea name="content" id="real_time_rec" placeholder="Scrivi la tua recensione (in lingua inglese)"class="autosize-transition form-control"></textarea>
									</form>
									<br>
										<div id="spinner_div" class="hidden">
											<div id="spinner_message">Sto eseguendo il calcolo...</div>
											<i  class="ace-icon fa fa-spinner fa-spin orange bigger-200 "></i>
										</div>
										<button id="submit_real_time" class="btn btn-white btn-info btn-bold" >
											<i class="ace-icon fa fa-cogs"></i>
											Calcola
										</button>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-3">
						<div class="widget-box">
							<div class="widget-header">
								<h4 class="smaller">VALORE STIMATO</h4>
							</div>

							<div class="widget-body">
								<div class="widget-main" id="val_stimato">
									-
								</div>
							</div>
						</div>
					</div>

					<%-- <form action="api/inline" method="POST" id="form-categoria">

						Inserire nuova recensione:<br /> <label>Recensione*</label>:
						<textarea rows="4" cols="50" name="content"
							placeholder="Inserire qui la recensione scritta in lingua inglese."
							required></textarea>
						<br /> <br /> <input type="button" id="submit"
							value="Inserisci recensione"> <input type="reset"
							value="Reset">
					</form> --%>
				</div>
			</div>
			<br>
			<br>
			<br>
		</div>
	</div>
	<script src="./assets/scripts/realtime.js"></script>
	<audio id="song"></audio>
</body>
</html>

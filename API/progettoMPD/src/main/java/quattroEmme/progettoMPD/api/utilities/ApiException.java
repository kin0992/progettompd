package quattroEmme.progettoMPD.api.utilities;

import quattroEmme.progettoMPD.api.models.ErrorMessage;

public class ApiException extends Exception {

	private static final long serialVersionUID = 1L;
	private ErrorMessage errorMessage;

	public ApiException() {
	}

	public ApiException(ErrorMessage errorMessage) {
		this.errorMessage = errorMessage;
	}

	public ErrorMessage getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(ErrorMessage errorMessage) {
		this.errorMessage = errorMessage;
	}
}

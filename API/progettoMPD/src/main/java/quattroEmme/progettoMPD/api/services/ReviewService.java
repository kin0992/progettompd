package quattroEmme.progettoMPD.api.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.mongodb.morphia.query.Query;

import quattroEmme.progettoMPD.api.models.ErrorMessage;
import quattroEmme.progettoMPD.api.models.Review;
import quattroEmme.progettoMPD.api.utilities.ApiException;

public class ReviewService extends BaseService {

	public List<Review> getAllReviewsOfHotel(int hotelId) throws ApiException {
		List<Review> reviews = getDatastore().createQuery(Review.class).field("hotel_id").equal(hotelId).asList();
		if (reviews.isEmpty()) {
			ErrorMessage errorMessage = new ErrorMessage(404, "Review not found");
			throw new ApiException(errorMessage);
		}
		return reviews;
	}

	public List<Review> getPositiveReviews(int hotelId) throws ApiException {
		Query<Review> query = getDatastore().find(Review.class);
		query.and(query.criteria("hotel_id").equal(hotelId), query.criteria("overall_review").greaterThanOrEq(4));
		List<Review> reviews = query.asList();
		if (reviews.isEmpty()) {
			ErrorMessage errorMessage = new ErrorMessage(404, "Review not found");
			throw new ApiException(errorMessage);
		}
		return reviews;
	}

	public List<Review> getPositiveReviewsAlgoritmo(int hotelId) throws ApiException {
		Query<Review> query = getDatastore().find(Review.class);
		query.and(query.criteria("hotel_id").equal(hotelId), query.criteria("overall_algoritmo").greaterThanOrEq(4));
		List<Review> reviews = query.asList();
		if (reviews.isEmpty()) {
			ErrorMessage errorMessage = new ErrorMessage(404, "Review not found");
			throw new ApiException(errorMessage);
		}
		return reviews;
	}

	public List<Review> getNeutralReviews(int hotelId) throws ApiException {
		Query<Review> query = getDatastore().find(Review.class);
		query.and(query.criteria("hotel_id").equal(hotelId), query.criteria("overall_review").equal(3));
		List<Review> reviews = query.asList();
		if (reviews.isEmpty()) {
			ErrorMessage errorMessage = new ErrorMessage(404, "Review not found");
			throw new ApiException(errorMessage);
		}
		return reviews;
	}

	public List<Review> getNeutralReviewsAlgoritmo(int hotelId) throws ApiException {
		Query<Review> query = getDatastore().find(Review.class);
		query.and(query.criteria("hotel_id").equal(hotelId), query.criteria("overall_algoritmo").equal(3));
		List<Review> reviews = query.asList();
		if (reviews.isEmpty()) {
			ErrorMessage errorMessage = new ErrorMessage(404, "Review not found");
			throw new ApiException(errorMessage);
		}
		return reviews;
	}

	public List<Review> getNegativeReviews(int hotelId) throws ApiException {
		Query<Review> query = getDatastore().find(Review.class);
		query.and(query.criteria("hotel_id").equal(hotelId), query.criteria("overall_review").lessThanOrEq(2));
		List<Review> reviews = query.asList();
		if (reviews.isEmpty()) {
			ErrorMessage errorMessage = new ErrorMessage(404, "Review not found");
			throw new ApiException(errorMessage);
		}
		return reviews;
	}

	public List<Review> getNegativeReviewsAlgoritmo(int hotelId) throws ApiException {
		Query<Review> query = getDatastore().find(Review.class);
		query.and(query.criteria("hotel_id").equal(hotelId), query.criteria("overall_algoritmo").lessThanOrEq(2));
		List<Review> reviews = query.asList();
		if (reviews.isEmpty()) {
			ErrorMessage errorMessage = new ErrorMessage(404, "Review not found");
			throw new ApiException(errorMessage);
		}
		return reviews;
	}

	public Review getSingleReviewOfHotel(int hotelId, int reviewId) throws ApiException {
		Query<Review> query = getDatastore().find(Review.class);
		query.and(query.criteria("hotel_id").equal(hotelId), query.criteria("review_id").equal(reviewId));
		Review review = query.get();
		if (review == null) {
			ErrorMessage errorMessage = new ErrorMessage(404, "Review not found");
			throw new ApiException(errorMessage);
		}
		return review;
	}

	public Map<Integer, Double> andamentoNelTempo(int hotelId) {
		Map<Integer, Double> tempoReview = new HashMap<Integer, Double>();
		List<Review> reviews = getDatastore().createQuery(Review.class).field("hotel_id").equal(hotelId).asList();
		Iterator<Review> re = reviews.iterator();
		Set<Integer> years = new HashSet<Integer>();
		while (re.hasNext()) {
			years.add(parseDate(re.next().getDate()));
		}
		Iterator<Integer> yearsIterator = years.iterator();
		while (yearsIterator.hasNext()) {
			int year = yearsIterator.next();
			List<Double> ratingsRev = new ArrayList<Double>();
			Iterator<Review> rev = reviews.iterator();
			while (rev.hasNext()) {
				Review review = rev.next();
				int anno = parseDate(review.getDate());
				if (anno == year) {
					ratingsRev.add(review.getOverall_review());
				}
			}
			if (ratingsRev.size() > 0) {
				double avg = ratingsRev.stream().reduce((a, b) -> a + b).get() / ratingsRev.size();
				tempoReview.put(year, avg);
			}
		}
		return tempoReview;
	}

	public Map<Integer, Double> andamentoNelTempoAlgoritmo(int hotelId) {
		Map<Integer, Double> tempoAlgoritmo = new HashMap<Integer, Double>();
		List<Review> reviews = getDatastore().createQuery(Review.class).field("hotel_id").equal(hotelId).asList();
		Iterator<Review> re = reviews.iterator();
		Set<Integer> years = new HashSet<Integer>();
		while (re.hasNext()) {
			years.add(parseDate(re.next().getDate()));
		}
		Iterator<Integer> yearsIterator = years.iterator();
		while (yearsIterator.hasNext()) {
			int year = yearsIterator.next();
			List<Double> ratingsAlg = new ArrayList<Double>();
			Iterator<Review> rev = reviews.iterator();
			while (rev.hasNext()) {
				Review review = rev.next();
				int anno = parseDate(review.getDate());
				if (anno == year) {
					ratingsAlg.add(review.getOverall_algoritmo());
				}
			}
			if (ratingsAlg.size() > 0) {
				double avg = ratingsAlg.stream().reduce((a, b) -> a + b).get() / ratingsAlg.size();
				tempoAlgoritmo.put(year, avg);
			}
		}
		return tempoAlgoritmo;
	}

	public double calcolaMediaHotelAlgoritmo(int hotelId) {
		List<Review> reviews = getDatastore().createQuery(Review.class).field("hotel_id").equal(hotelId).asList();
		List<Double> valutazioni = new ArrayList<Double>();
		Iterator<Review> re = reviews.iterator();
		while (re.hasNext()) {
			valutazioni.add(re.next().getOverall_algoritmo());
		}
		double avgHotel = 0;
		if (valutazioni.size() > 0) {
			avgHotel = valutazioni.stream().reduce((a, b) -> a + b).get() / valutazioni.size();
		}
		return avgHotel;
	}

	private int parseDate(String date) {
		int index = date.indexOf(",");
		date = date.substring(index + 2);
		return Integer.valueOf(date);
	}

}

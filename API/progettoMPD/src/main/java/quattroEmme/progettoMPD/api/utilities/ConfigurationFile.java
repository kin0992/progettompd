package quattroEmme.progettoMPD.api.utilities;

public class ConfigurationFile {

	public static final String SERVER_NAME = "localhost";
	public static final String DB_NAME = "progettoMPD";
	public static final int DB_PORT = 27017;

}

package quattroEmme.progettoMPD.api.services;

import java.util.ArrayList;
import java.util.List;

import quattroEmme.progettoMPD.api.models.ErrorMessage;
import quattroEmme.progettoMPD.api.models.InlineReview;
import quattroEmme.progettoMPD.api.utilities.ApiException;

public class InlineReviewService extends BaseService {

	public InlineReviewService() {
	}

	public InlineReview insertReview(String content) {
		InlineReview inlineReview = new InlineReview(reviewNumber(), content);
		getDatastore().save(inlineReview);
		return inlineReview;
	}

	public List<InlineReview> getAllReviews() {
		List<InlineReview> list = getDatastore().createQuery(InlineReview.class).asList();
		return list;
	}

	public InlineReview checkReview(int reviewId) {
		try {
			boolean valid = false;
			InlineReview review = new InlineReview();
			while (valid == false) {
				review = getDatastore().createQuery(InlineReview.class).field("review_id").equal(reviewId).get();
				if (review == null) {
					throw new ApiException(new ErrorMessage(404, "Recensione non trovata"));
				}
				if (review.getOverall_algo() > 0)
					valid = true;
			}
			return review;
		} catch (ApiException e) {
			InlineReview review = checkReview(reviewId);
			return review;
		}
	}

	public InlineReview getReview(int reviewId) {
		InlineReview inlineReview = getDatastore().createQuery(InlineReview.class).field("review_id").equal(reviewId)
				.get();
		return inlineReview;
	}

	private int reviewNumber() {
		List<InlineReview> reviews = getDatastore().createQuery(InlineReview.class).asList();
		if (reviews.isEmpty())
			return 1;
		else {
			List<Integer> ids = new ArrayList<>();
			reviews.stream().forEach(r -> ids.add(r.getReview_id()));
			int max = ids.stream().max(Integer::compare).get();
			for (int i = 1; i <= max; i++) {
				if (!ids.contains(i))
					return i;
			}
			return max + 1;
		}
	}

}

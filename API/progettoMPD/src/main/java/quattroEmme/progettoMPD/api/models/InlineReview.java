package quattroEmme.progettoMPD.api.models;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity(value = "review_testing_run_time", noClassnameStored = true)
public class InlineReview {

	@Id
	private ObjectId id;
	private int review_id;
	private String content;
	private double overall_algo;
	private boolean calcolato;

	public InlineReview() {
	}

	public InlineReview(int review_id, String content, double overall) {
		this.review_id = review_id;
		this.content = content;
		this.overall_algo = overall;
		this.calcolato = false;
	}

	public InlineReview(int review_id, String content) {
		this.review_id = review_id;
		this.content = content;
	}

	public int getReview_id() {
		return review_id;
	}

	public void setReview_id(int review_id) {
		this.review_id = review_id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public double getOverall_algo() {
		return overall_algo;
	}

	public void setOverall_algo(double overall_algo) {
		this.overall_algo = overall_algo;
	}

	public boolean isCalcolato() {
		return calcolato;
	}

	public void setCalcolato(boolean calcolato) {
		this.calcolato = calcolato;
	}

}

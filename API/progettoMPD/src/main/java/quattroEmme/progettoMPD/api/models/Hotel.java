package quattroEmme.progettoMPD.api.models;

import java.util.Map;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity("hotel_testing")
public class Hotel {

	@Id
	private ObjectId id;
	private int index;
	private String name;
	private String place;
	private double rating_testing;
	private double rating_algoritmo;
	private String avg_prize;
	private String url;
	private Map<Integer, Double> andamento_review;
	private Map<Integer, Double> andamento_algoritmo;
	private double correttezza;

	public Hotel() {
	}

	public Hotel(int index, String name, String place, double rating_t, double rating_a, String prize, String url) {
		this.index = index;
		this.name = name;
		this.place = place;
		this.rating_testing = rating_t;
		this.rating_algoritmo = rating_a;
		this.avg_prize = prize;
		this.url = url;
	}

	public ObjectId getId() {
		return id;
	}

	public int getIndex() {
		return index;
	}

	public String getName() {
		return name;
	}

	public String getPlace() {
		return place;
	}

	public double getRating_testing() {
		return rating_testing;
	}

	public double getRating_algoritmo() {
		return rating_algoritmo;
	}

	public String getAvg_prize() {
		return avg_prize;
	}

	public String getUrl() {
		return url;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public void setRating_testing(double rating_testing) {
		this.rating_testing = rating_testing;
	}

	public void setRating_algoritmo(double rating_algoritmo) {
		this.rating_algoritmo = rating_algoritmo;
	}

	public void setAvg_prize(String avg_prize) {
		this.avg_prize = avg_prize;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Map<Integer, Double> getAndamento_review() {
		return andamento_review;
	}

	public void setAndamento_review(Map<Integer, Double> andamento_review) {
		this.andamento_review = andamento_review;
	}

	public Map<Integer, Double> getAndamento_algoritmo() {
		return andamento_algoritmo;
	}

	public void setAndamento_algoritmo(Map<Integer, Double> andamento_algoritmo) {
		this.andamento_algoritmo = andamento_algoritmo;
	}

	public double getCorrettezza() {
		return correttezza;
	}

	public void setCorrettezza(double correttezza) {
		this.correttezza = correttezza;
	}
}

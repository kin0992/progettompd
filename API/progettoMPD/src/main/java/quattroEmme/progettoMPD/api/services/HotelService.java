package quattroEmme.progettoMPD.api.services;

import java.util.List;

import quattroEmme.progettoMPD.api.models.ErrorMessage;
import quattroEmme.progettoMPD.api.models.Hotel;
import quattroEmme.progettoMPD.api.models.Review;
import quattroEmme.progettoMPD.api.utilities.ApiException;

public class HotelService extends BaseService {

	public List<Hotel> getAllHotels() throws ApiException {
		List<Hotel> hotels = getDatastore().createQuery(Hotel.class).asList();
		if (hotels.isEmpty())
			throw new ApiException(new ErrorMessage(404, "No hotels found"));
		return hotels;
	}

	public List<Hotel> getHotelRatingGreatherThan(int rating) throws ApiException {
		if (rating <= 0)
			throw new ApiException(new ErrorMessage(400, "Rating inserito non valido"));
		if (rating > 5)
			throw new ApiException(new ErrorMessage(400, "Rating inserito non valido"));
		List<Hotel> hotels = getDatastore().createQuery(Hotel.class).field("rating").greaterThan(rating).asList();
		if (hotels.isEmpty())
			throw new ApiException(new ErrorMessage(404, "Hotel not found"));
		return hotels;

	}

	public Hotel getHotelByName(String hotelName) throws ApiException {
		Hotel hotel = getDatastore().createQuery(Hotel.class).field("name").equal(hotelName).get();
		if (hotel == null)
			throw new ApiException(new ErrorMessage(404, "Hotel not found"));
		return hotel;
	}

	public Hotel getHotelByIndex(int hotelIndex) throws ApiException {
		Hotel hotel = getDatastore().createQuery(Hotel.class).field("index").equal(hotelIndex).get();
		if (hotel == null)
			throw new ApiException(new ErrorMessage(404, "Hotel not found"));
		return hotel;
	}

	public double calcolaCorrettezza(int hotelId) {
		List<Review> reviews = getDatastore().createQuery(Review.class).field("hotel_id").equal(hotelId).asList();
		double cont = 0;
		for (Review r : reviews) {
			if (Math.abs(r.getOverall_algoritmo() - r.getOverall_review()) <= 1)
				cont++;
		}
		return cont / reviews.size() * 100;
	}

}

package quattroEmme.progettoMPD.api.models;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity("review_testing")
public class Review {

	public Review() {
	}

	@Id
	private ObjectId id;
	private int review_id;
	private int hotel_id;
	private String author;
	private String content;
	private String date;
	private double overall_review;
	private double overall_algoritmo;
	private double rooms;
	private double location;
	private double cleanliness;
	private double check_in;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public int getReview_id() {
		return review_id;
	}

	public void setReview_id(int review_id) {
		this.review_id = review_id;
	}

	public int getHotel_id() {
		return hotel_id;
	}

	public void setHotel_id(int hotel_id) {
		this.hotel_id = hotel_id;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public double getOverall_review() {
		return overall_review;
	}

	public void setOverall_review(double overall_review) {
		this.overall_review = overall_review;
	}

	public double getOverall_algoritmo() {
		return overall_algoritmo;
	}

	public void setOverall_algoritmo(double overall_algoritmo) {
		this.overall_algoritmo = overall_algoritmo;
	}

	public double getRooms() {
		return rooms;
	}

	public void setRooms(double rooms) {
		this.rooms = rooms;
	}

	public double getLocation() {
		return location;
	}

	public void setLocation(double location) {
		this.location = location;
	}

	public double getCleanliness() {
		return cleanliness;
	}

	public void setCleanliness(double cleanliness) {
		this.cleanliness = cleanliness;
	}

	public double getCheck_in() {
		return check_in;
	}

	public void setCheck_in(double check_in) {
		this.check_in = check_in;
	}

}

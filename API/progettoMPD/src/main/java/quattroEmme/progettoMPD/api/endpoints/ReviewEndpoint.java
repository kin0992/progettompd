package quattroEmme.progettoMPD.api.endpoints;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import quattroEmme.progettoMPD.api.models.ErrorMessage;
import quattroEmme.progettoMPD.api.models.Review;
import quattroEmme.progettoMPD.api.services.ReviewService;
import quattroEmme.progettoMPD.api.utilities.ApiException;

@Path("/hotels/{hotelId}/reviews")
@Produces(MediaType.APPLICATION_JSON)
public class ReviewEndpoint {

	ReviewService rs = new ReviewService();

	/*
	 * Chiamata API che recupera tutte le recensioni di un hotel 
	 * Path: /hotels/{numeroHotel}/reviews
	 */
	@GET
	public Response getAllReviewsOfHotel(@PathParam("hotelId") int hotelId) {
		try {
			List<Review> reviews = new ArrayList<Review>();
			reviews = rs.getAllReviewsOfHotel(hotelId);
			return Response.ok(reviews.toArray(new Review[0])).build();
		} catch (ApiException e) {
			ErrorMessage errorMessage = e.getErrorMessage();
			throw new WebApplicationException(Response.status(errorMessage.getCode()).entity(errorMessage).build());
		}
	}

	/*
	 * Chiamata API che recupera una determinata recensione di uno specifico
	 * hotel 
	 * Path: /hotels/{numeroHotel}/reviews/{numeroRecensione}
	 */
	@GET
	@Path("{reviewId}")
	public Response getSingleReviewOfHotel(@PathParam("hotelId") int hotelId, @PathParam("reviewId") int reviewId) {
		try {
			Review review = rs.getSingleReviewOfHotel(hotelId, reviewId);
			return Response.ok(review).build();
		} catch (ApiException e) {
			ErrorMessage errorMessage = e.getErrorMessage();
			throw new WebApplicationException(Response.status(errorMessage.getCode()).entity(errorMessage).build());
		}
	}

	/*
	 * Chiamata API che recupera tutte le recensioni positive di un hotel; se il
	 * query param è nullo, recupera le recensioni positive dell'hotel definite
	 * dall'utente (quelle nelle recensioni), altrimenti, se il query param è un
	 * qualsiasi numero, recupera le recensioni positive calcolate
	 * dall'algoritmo le recensioni positive sono quelle con rating > 3 
	 * Path: /hotels/{numeroHotel}/reviews/positive 
	 * 					oppure
	 * /hotels/{numeroHotel}/reviews/positive?algoritmo=1
	 */
	@GET
	@Path("positive")
	public Response getAllPositiveReviews(@PathParam("hotelId") int hotelId, @QueryParam("algoritmo") int algoritmo) {
		try {
			List<Review> reviews = new ArrayList<Review>();
			if (algoritmo == 0)
				reviews = rs.getPositiveReviews(hotelId);
			else
				reviews = rs.getPositiveReviewsAlgoritmo(hotelId);
			return Response.ok(reviews.toArray(new Review[0])).build();
		} catch (ApiException e) {
			ErrorMessage errorMessage = e.getErrorMessage();
			throw new WebApplicationException(Response.status(errorMessage.getCode()).entity(errorMessage).build());
		}
	}

	/*
	 * Chiamata API che recupera tutte le recensioni neutre di un hotel; se il
	 * query param è nullo, recupera le recensioni neutre dell'hotel definite
	 * dall'utente (quelle nelle recensioni), altrimenti, se il query param è un
	 * qualsiasi numero, recupera le recensioni neutre calcolate dall'algoritmo
	 * le recensioni neutre sono quelle con rating = 3 
	 * Path: /hotels/{numeroHotel}/reviews/neutral 
	 * 				oppure
	 * /hotels/{numeroHotel}/reviews/neutral?algoritmo=1
	 */
	@GET
	@Path("neutral")
	public Response getAllNeutralReviews(@PathParam("hotelId") int hotelId, @QueryParam("algoritmo") int algoritmo) {
		try {
			List<Review> reviews = new ArrayList<Review>();
			if (algoritmo == 0)
				reviews = rs.getNeutralReviews(hotelId);
			else
				reviews = rs.getNeutralReviewsAlgoritmo(hotelId);
			return Response.ok(reviews.toArray(new Review[0])).build();
		} catch (ApiException e) {
			ErrorMessage errorMessage = e.getErrorMessage();
			throw new WebApplicationException(Response.status(errorMessage.getCode()).entity(errorMessage).build());
		}
	}

	/*
	 * Chiamata API che recupera tutte le recensioni negative di un hotel; se il
	 * query param è nullo, recupera le recensioni negative dell'hotel definite
	 * dall'utente (quelle nelle recensioni), altrimenti, se il query param è un
	 * qualsiasi numero, recupera le recensioni negative calcolate
	 * dall'algoritmo le recensioni negative sono quelle con rating < 3 
	 * Path: /hotels/{numeroHotel}/reviews/negative 
	 * 					oppure
	 * /hotels/{numeroHotel}/reviews/negative?algoritmo=1
	 */
	@GET
	@Path("negative")
	public Response getAllNegativeReviews(@PathParam("hotelId") int hotelId, @QueryParam("algoritmo") int algoritmo) {
		try {
			List<Review> reviews = new ArrayList<Review>();
			if (algoritmo == 0)
				reviews = rs.getNegativeReviews(hotelId);
			else
				reviews = rs.getNegativeReviewsAlgoritmo(hotelId);
			return Response.ok(reviews.toArray(new Review[0])).build();
		} catch (ApiException e) {
			ErrorMessage errorMessage = e.getErrorMessage();
			throw new WebApplicationException(Response.status(errorMessage.getCode()).entity(errorMessage).build());
		}
	}

}

package quattroEmme.progettoMPD.api.endpoints;

import java.net.URI;
import java.util.List;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import quattroEmme.progettoMPD.api.models.InlineReview;
import quattroEmme.progettoMPD.api.services.InlineReviewService;
import quattroEmme.progettoMPD.api.utilities.ApiException;

@Path("inline")
@Produces(MediaType.APPLICATION_JSON)
public class InlineReviewEndpoint {

	InlineReviewService irs = new InlineReviewService();

	/*
	 * Chiamata API per inserire una recensione in real time dall'applicazione
	 * web 
	 * Path: /inline
	 */
	@POST
	public Response insertReview(@FormParam("content") String content, @Context UriInfo uriInfo) {
		InlineReview ir = irs.insertReview(content);
		URI location = uriInfo.getAbsolutePathBuilder().path(String.valueOf(ir.getReview_id())).build();
		return Response.created(location).build();
	}

	/*
	 * Chiamata API per recuperare tutte le recensioni inserite in real time
	 * Path: /inline
	 */
	@GET
	public Response getAllReviews() {
		List<InlineReview> ir = irs.getAllReviews();
		return Response.ok(ir.toArray(new InlineReview[0])).build();
	}

	/*
	 * Chiamata API per recuperare la recensione inserita dopo che è stato
	 * calcolato il sentiment con l'algoritmo 
	 * Path:/inline/waiting/{numeroRecensione}
	 */
	@GET
	@Path("waiting/{reviewId}")
	public Response waitResult(@PathParam("reviewId") int reviewId) throws ApiException {
		InlineReview ir = irs.checkReview(reviewId);
		return Response.ok(ir).build();
	}

	/*
	 * Chiamata API per recuperare una recensione inserita in real time dato il
	 * suo numero 
	 * Path: /inline/{numeroRecensione}
	 */
	@GET
	@Path("{reviewId}")
	public Response getReviewById(@PathParam("reviewId") int reviewId) {
		InlineReview ir = irs.getReview(reviewId);
		return Response.ok(ir).build();
	}

}

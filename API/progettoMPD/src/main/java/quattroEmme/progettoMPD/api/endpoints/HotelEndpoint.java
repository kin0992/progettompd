package quattroEmme.progettoMPD.api.endpoints;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import quattroEmme.progettoMPD.api.models.ErrorMessage;
import quattroEmme.progettoMPD.api.models.Hotel;
import quattroEmme.progettoMPD.api.services.HotelService;
import quattroEmme.progettoMPD.api.services.ReviewService;
import quattroEmme.progettoMPD.api.utilities.ApiException;

@Produces(MediaType.APPLICATION_JSON)
@Path("hotels")
public class HotelEndpoint {

	HotelService hs = new HotelService();
	ReviewService rs = new ReviewService();

	/*
	 * chiamata API che recupera tutti gli hotel: se il query param è impostato
	 * recupera gli hotel con un rating maggiore del valore impostato 
	 * Path: /hotels oppure /hotels?rating=1
	 */
	@GET
	public Response getAllHotels(@QueryParam("rating") int rating) {
		try {
			List<Hotel> hotels = new ArrayList<Hotel>();
			if (rating != 0)
				hotels = hs.getHotelRatingGreatherThan(rating);
			else
				hotels = hs.getAllHotels();
			return Response.ok(hotels.toArray(new Hotel[0])).build();
		} catch (ApiException e) {
			ErrorMessage errorMessage = e.getErrorMessage();
			throw new WebApplicationException(Response.status(errorMessage.getCode()).entity(errorMessage).build());
		}
	}

	/*
	 * chiamata API che recupera un hotel ricercandolo per nome 
	 * Path: /hotels/{nomeHotel}
	 */
	@GET
	@Path("{hotelName}")
	public Response getHotelByName(@PathParam("hotelName") String hotelName) {
		try {
			Hotel hotel = hs.getHotelByName(hotelName);
			hotel.setAndamento_review(rs.andamentoNelTempo(hotel.getIndex()));
			hotel.setAndamento_algoritmo(rs.andamentoNelTempoAlgoritmo(hotel.getIndex()));
			hotel.setRating_algoritmo(rs.calcolaMediaHotelAlgoritmo(hotel.getIndex()));
			hotel.setCorrettezza(hs.calcolaCorrettezza(hotel.getIndex()));
			return Response.ok(hotel).build();
		} catch (ApiException e) {
			ErrorMessage errorMessage = e.getErrorMessage();
			throw new WebApplicationException(Response.status(errorMessage.getCode()).entity(errorMessage).build());
		}
	}

	/*
	 * chiamata API che un hotel scelto a caso 
	 * Path: hotels/random/{numeroRandom}
	 */
	@GET
	@Path("random/{hotelIndex}")
	public Response getHotelByName(@PathParam("hotelIndex") int hotelIndex) {
		try {
			Hotel hotel = hs.getHotelByIndex(hotelIndex);
			hotel.setAndamento_algoritmo(rs.andamentoNelTempoAlgoritmo(hotel.getIndex()));
			hotel.setAndamento_review(rs.andamentoNelTempo(hotel.getIndex()));
			hotel.setRating_algoritmo(rs.calcolaMediaHotelAlgoritmo(hotel.getIndex()));
			hotel.setCorrettezza(hs.calcolaCorrettezza(hotel.getIndex()));
			return Response.ok(hotel).build();
		} catch (ApiException e) {
			ErrorMessage errorMessage = e.getErrorMessage();
			throw new WebApplicationException(Response.status(errorMessage.getCode()).entity(errorMessage).build());
		}
	}
}

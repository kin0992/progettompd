package quattroEmme.progettoMPD.api.services;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import static quattroEmme.progettoMPD.api.utilities.ConfigurationFile.DB_NAME;
import static quattroEmme.progettoMPD.api.utilities.ConfigurationFile.DB_PORT;
import static quattroEmme.progettoMPD.api.utilities.ConfigurationFile.SERVER_NAME;

import com.mongodb.MongoClient;

public class BaseService {

	private static Datastore datastore;

	protected Datastore getDatastore() {
		if (datastore == null) {
			Morphia morphia = new Morphia();
			morphia.mapPackage("quattroEmme.progettoMPD.api.models");
			datastore = morphia.createDatastore(new MongoClient(SERVER_NAME, DB_PORT), DB_NAME);
		}
		return datastore;
	}

}

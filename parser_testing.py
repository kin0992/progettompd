from langdetect import detect
import os
from hotel_testing import *


# esegue il parsing delle recensioni in testing e le inserisce nel db
folder_name = 'data/Dataset/Testing'
root_dir = os.path.realpath(folder_name)
root_dir += "/"
index_hotel = 1
index_rev = 1
for subdir, dirs, files in os.walk(root_dir):
    for file in files:
        # print file
        path = subdir + file
        in_file = open(path, "r")
        first = True
        header = {}
        recensione = []
        D = {}
        for line in in_file:
            # riga vuota
            if line in ['\n', '\r\n']:
                # lette le prime tre righe
                if first:
                    first = False
                    # print D #DEBUG
                    header = D
                    D = {}
                    # completata una recensione
                else:
                    # print D #DEBUG
                    try:
                        if 'URL' in header:
                            if 'Content' in D:
                                if detect(D['Content']) == 'en':  # salva solo le recensioni in inglese
                                    if not 'showReview(' in D['Content']:  # non salva le recensioni con riferimenti
                                        D["index_rev"] = index_rev
                                        D['Content'] = D['Content'].lower()
                                        recensione += [D]
                                        insert_review_data(index_rev, index_hotel, str(D['Author']), D['Content'], D['Date'],
                                                           float(D['Overall']), float(D['Rooms']), float(D['Location']),
                                                           float(D['Cleanliness']), float(D['Check_in_/_front_desk']), float(D['Service']))
                                        # print D['Author']
                                        index_rev += 1
                                        # print recensione
                                        D = {}
                                else:
                                    D = {}
                    except (RuntimeError, UnicodeDecodeError, KeyError):
                        D = {}
            else:
                if "img" in line:
                    D["img"] = line[line.find("\""): line.find("alt")].strip().replace("\"", "")
                    # print D["img"] #DEBUG
                else:
                    # substring dal secondo carattere (salto <) al primo tag di chiusura per la chiave
                    # substring dal primo tag di chiusura (escluso) fino alla fine togliendo gli spazi agli estremi per il valore
                    D[line[1:line.find(">")].replace(".", "").replace(" ", "_")] = line[line.find(">") + 1: len(line)].strip()
        if 'URL' in header:
            dash_index = header['URL'].rfind("-")
            header['place'] = header['URL'][dash_index + 1: header['URL'].rfind('.')].replace('_', ' ')
            substring = header['URL'][0: header['URL'].rfind("-")]
            header['name'] = substring[substring.rfind('-') + 1: len(substring)].replace('_', ' ')
            insert_hotel_data(index_hotel, header['name'], header['place'], float(header['Overall_Rating']),
                                header['Avg_Price'], str(header['URL']))
            index_hotel += 1
        # print file #DEBUG
        # print header
        # print recensione
        print (float(index_hotel)/300)*100
        in_file.close()
        # break #DEBUG

from review_saver import insert_data
from langdetect import detect
from bigrams_saver import insert_bigrams
import os


# esegue il parsing delle recensioni in training e le inserisce nel db
folder_name = 'data/Dataset/Training'
root_dir = os.path.realpath(folder_name)
root_dir = root_dir + "/"
index_hotel = 0
index_rev = 1
for subdir, dirs, files in os.walk(root_dir):
    for file in files:
        print file
        index_hotel += 1
        path = subdir + file
        in_file = open(path, "r")
        first = True
        header = {}
        recensione = []
        D = {}
        for line in in_file:
            # riga vuota
            if line in ['\n', '\r\n']:
                # lette le prime tre righe
                if first:
                    first = False
                    # print D #DEBUG
                    header = D
                    D = {}
                    # completata una recensione
                else:
                    # print D #DEBUG
                    try:
                        if 'Content' in D:
                            if detect(D['Content']) == 'en': #salva solo le recensioni in inglese
                                if not 'showReview(' in D['Content']: #non salva le recensioni con riferimenti
                                    D["index_rev"] = index_rev
                                    D['Content'] = D['Content'].lower()
                                    insert_bigrams(D["index_rev"], D['Content'], D['Overall'])
                                    index_rev += 1
                                    recensione += [D]
                                    D = {}
                            else:
                                D = {}
                    except (RuntimeError, UnicodeDecodeError):
                        D = {}
            else:
                if "img" in line:
                    D["img"] = line[line.find("\""): line.find("alt")].strip().replace("\"", "")
                    # print D["img"] #DEBUG
                else:
                    # substring dal secondo carattere (salto <) al primo tag di chiusura per la chiave
                    # substring dal primo tag di chiusura (escluso) fino alla fine togliendo gli spazi agli estremi per il valore
                    D[line[1:line.find(">")].replace(".", "").replace(" ", "_")] = line[line.find(">") + 1: len(line)].strip()
        if 'URL' not in header:
            header['URL'] = "NULL"
            header['place'] = "NULL"
            header['name'] = "NULL"
        else:
            dash_index = header['URL'].rfind("-")
            header['place'] = header['URL'][dash_index+1: header['URL'].rfind('.')].replace('_', ' ')
            substring = header['URL'][0: header['URL'].rfind("-")]
            header['name'] = substring[substring.rfind('-')+1: len(substring)].replace('_', ' ')
        # print file #DEBUG
        insert_data(index_hotel, header['name'], header['place'], float(header['Overall_Rating']), header['Avg_Price'], str(header['URL']), recensione)
        # print recensione
        in_file.close()
        # break #DEBUG

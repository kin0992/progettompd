# coding=utf-8
from stemming.porter2 import stem
from nltk.corpus import stopwords
from nltk.stem.porter import re


# data in input una frase restituisce i bigrammi corrispondenti
def find_bigrams(input_list):
    return zip(input_list, input_list[1:])


# elimina i caratteri speciali anche ripetuti da un testo che riceve in input
def replace_char(text):
    text = text.replace("(", "")\
        .replace(")", "")\
        .replace("-", " ")\
        .replace("[", "")\
        .replace("]", "")\
        .replace("%", "")\
        .replace("'", "")\
        .replace("\"", "")\
        .replace("£", "")\
        .replace("€", "")\
        .replace("^", "")\
        .replace("@", "")\
        .replace("$", "")\
        .replace("<", "")\
        .replace(">", "")\
        .replace("#", "")\
        .replace("+", "")\
        .replace("/", " ")\
        .replace("&", " ")
    text = re.sub("[0-9]*", '', text)
    text = re.split(', |[!]+ |[?]+ |; |[.]+', text)
    text[len(text) - 1] = text[len(text) - 1].replace("!", "")
    # rimuovo eventuali frasi vuote
    text = filter(None, text)
    return text


# elimina le stop word da un testo ricevuto in input
def stop_word_list(text):
    # carico stopword list
    stop_word_list = stopwords.words('english')
    # tolte le stopwords
    for phrase in text:
        index = text.index(unicode(phrase))
        text[index] = " ".join(i for i in phrase.split() if i.lower() not in stop_word_list)
    return text


# esegue lo stemming su un testo ricevuto in input
def stemming(text):
    for phrase in text:
        p2 = ''
        for word in phrase.split():
            p2 += stem(word) + ' '
        index = text.index(phrase)
        text[index] = p2.strip()
    return text


# dato un testo crea i bigrammi corrispondenti
def create_bigrams(text):
    bigrams = []
    for phrase in text:
        bigrams.append(find_bigrams(phrase.split()))
    return bigrams


# funzione che dato un testo esegue le operazioni necessarie per creare i bigrammi e ritorna una lista di bigrammi
def init(text):
    text = replace_char(text)
    text = stop_word_list(text)
    text = stemming(text)
    text = create_bigrams(text)
    text = [y for x in text for y in x]
    return filter(None, text)

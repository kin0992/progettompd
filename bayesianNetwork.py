# coding=utf-8
import os
from libpgm.nodedata import NodeData
from libpgm.graphskeleton import GraphSkeleton
from libpgm.discretebayesiannetwork import DiscreteBayesianNetwork
from libpgm.tablecpdfactorization import TableCPDFactorization

from hotel_testing import *
from bigrams import *

# classe che definisce l'oggetto da inserire nel db per la valutazione real time
class Review_testing_run_time(Document):
    review_id = IntField()
    content = StringField()
    overall_algo = FloatField()
    calcolato = BooleanField()


#  Funzione che carica la rete bayesiana in memoria leggendo il JSON
def load_network():
    nd = NodeData()
    skel = GraphSkeleton()
    folder_name = ''
    rootdir = os.path.realpath(folder_name)
    rootdir += "/2k.txt"
    nd.load(rootdir)  # any input file
    skel.load(rootdir)
    skel.toporder()  # definisce ordine topologico rete
    bn = DiscreteBayesianNetwork(skel, nd)  # creazione rete bayesiana
    return bn


# Prende le recensioni di testing e calcola il sentiment più probabile usando la fattorizzazione
def factorization(rev_index_start, rev_index_stop):
    bn = load_network()
    for i in range(rev_index_start, rev_index_stop):  # cicla sulle recensioni di testing
        evidence = {}
        evidence = evidence.fromkeys(bn.V, '0')  # dizionario con le evidenze

        recensione = Review_testing.objects(review_id=i).scalar('content')  # prende recensione dal db
        bigrams = init(str(recensione[0]))  # bigrammi presenti nella recensione

        # crea una lista di stringhe conformi alla rete
        string_bigrams = []
        for bigram in bigrams:
            string_bigrams.append("".join([bigram[0], bigram[1]]))

        # cerca il bigramma nella rete, se è presente fissa l'evidenza a 1 altrimenti rimane a 0
        for bigram in string_bigrams:
            if evidence.has_key(bigram):
                evidence[bigram] = '1'

        # elimina dalle evidenze il nodo radice, dato che serve per la query
        del evidence['S']

        # creazione dizionari query
        query_over_1 = dict(S='1')
        query_over_2 = dict(S='2')
        query_over_3 = dict(S='3')
        query_over_4 = dict(S='4')
        query_over_5 = dict(S='5')

        switch = {}
        fn = TableCPDFactorization(bn)
        # calcola la probabilita della query date le evidenze
        switch['1'] = fn.specificquery(query_over_1, evidence)

        fn = TableCPDFactorization(bn)
        switch['2'] = fn.specificquery(query_over_2, evidence)

        fn = TableCPDFactorization(bn)
        switch['3'] = fn.specificquery(query_over_3, evidence)

        fn = TableCPDFactorization(bn)
        switch['4'] = fn.specificquery(query_over_4, evidence)

        fn = TableCPDFactorization(bn)
        switch['5'] = fn.specificquery(query_over_5, evidence)

        max_sentiment = max(switch, key=switch.get)

        # inserisce il valore di sentiment calcolato nel database
        Review_testing.objects(review_id=i).modify(overall_algoritmo=max_sentiment)
        print "Recensione n°: %s su %s" % (i, rev_index_stop)


# funzione che fa partire il calcolo del sentiment per un range di recensioni
def start_inference():
    rev_index = Review_testing.objects().count()  # num di recensioni totali in testing
    rev_index += 1
    print "Numero recensioni: %s" % (rev_index)
    factorization(34628, rev_index)


# valuta la precisione del calcolo del sentiment
def valuta():
    lista = Review_testing.objects()
    count_esatto = 0
    count_approssimato = 0
    for item in lista:
        if item.overall_algoritmo == item.overall_review:
            count_esatto += 1
        if abs(item.overall_algoritmo - item.overall_review) <= 1:
            count_approssimato += 1
    print float(count_esatto)/len(lista)*100
    print float(count_approssimato)/len(lista)*100


# funzione per il calcolo del sentiment in real time
def factorization_real_time(bn, content):
    evidence = {}
    evidence = evidence.fromkeys(bn.V, '0')  # dizionario con le evidenze

    bigrams = init(str(content))  # bigrammi presenti nella recensione
    print bigrams
    # crea una lista di stringhe conformi alla rete
    string_bigrams = []
    for bigram in bigrams:
        string_bigrams.append("".join([bigram[0], bigram[1]]))

    for bigram in string_bigrams:
        if evidence.has_key(bigram):
            evidence[bigram] = '1'

    del evidence['S']

    # creazione dizionari query
    query_over_1 = dict(S='1')
    query_over_2 = dict(S='2')
    query_over_3 = dict(S='3')
    query_over_4 = dict(S='4')
    query_over_5 = dict(S='5')

    switch = {}
    fn = TableCPDFactorization(bn)
    # calcola la probabilita della query date le evidenze
    switch['1'] = fn.specificquery(query_over_1, evidence)

    fn = TableCPDFactorization(bn)
    switch['2'] = fn.specificquery(query_over_2, evidence)

    fn = TableCPDFactorization(bn)
    switch['3'] = fn.specificquery(query_over_3, evidence)

    fn = TableCPDFactorization(bn)
    switch['4'] = fn.specificquery(query_over_4, evidence)

    fn = TableCPDFactorization(bn)
    switch['5'] = fn.specificquery(query_over_5, evidence)

    max_sentiment = max(switch, key=switch.get)

    return max_sentiment

# coding=utf-8
from mongoengine import *

connect('progettoMPD')

# classe per gli hotel di testing che verrano inseriti nel db
class Hotel_testing(Document):
    index = IntField()
    name = StringField()
    place = StringField()
    rating_testing = FloatField()
    rating_algoritmo = FloatField()
    avg_prize = StringField()
    url = StringField()


# classe per le revisioni di testing che verrano inseriti nel db
class Review_testing(Document):
    review_id = IntField()
    hotel_id = IntField()
    author = StringField()
    content = StringField()
    date = StringField()
    overall_review = FloatField()
    overall_algoritmo = FloatField()
    rooms = FloatField()
    location = FloatField()
    cleanliness = FloatField()
    check_in = FloatField()
    service = FloatField()


# funzione che riceve in input i dati necessari, crea il documento hotel e lo salva
def insert_hotel_data(index, name, place, rating, avg_prize, url):
    hotel = Hotel_testing()
    hotel.index = index
    hotel.name = name
    hotel.place = place
    hotel.rating_testing = rating
    hotel.rating_algoritmo = 0
    hotel.avg_prize = avg_prize
    hotel.url = url
    hotel.save()


# funzione che riceve in input i dati necessari, crea il documento recensioni e lo salva
def insert_review_data(review_id, hotel_id, author, content, date, overall, rooms, location, cleanliness, check_in,
                       service):
    review = Review_testing()
    review.review_id = review_id
    review.hotel_id = hotel_id
    review.author = author
    review.content = content
    review.date = date
    review.overall_review = overall
    review.overall_algoritmo = 0
    review.rooms = rooms
    review.location = location
    review.cleanliness = cleanliness
    review.check_in = check_in
    review.service = service
    review.save()

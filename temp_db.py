from mongoengine import *


connect('progettoMPD')


# classe per i bigrammi univoci che verrano inseriti nel db
class Unique_bigrams(Document):
    bigrams = ListField()
    frequency = IntField()
    tfidf = FloatField()


# funzione che riceve in input un bigramma la frequenza e tfidf, crea il documento bigrammi univoci e lo salva
def insert_unique_bigrams(bigrammi, frequenza, tfidf):
    review_bigrams = Unique_bigrams()
    review_bigrams.bigrams = bigrammi
    review_bigrams.frequency = frequenza
    review_bigrams.tfidf = tfidf
    review_bigrams.save()

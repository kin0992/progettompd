# coding=utf-8
from mongoengine import *

connect('progettoMPD')


#definisco classe per hotel: Document indica che sarà il documento principale in mongo
class Hotel(Document):
    index = IntField()
    name = StringField()
    place = StringField()
    rating = FloatField()
    avg_prize = StringField()
    url = StringField()
    reviews = ListField(DictField())
    meta = {'allow_inheritance': True}


#definisco classe per hotel: Embedded indica che sarà un documento innestato nel documento principale (HOTEL)
class Review(EmbeddedDocument):
    review_id = IntField()
    author = StringField()
    content = StringField()
    date = DateTimeField()
    image = StringField()
    n_reader = IntField()
    n_helper = IntField()
    overall = IntField()
    value = IntField()
    rooms = IntField()
    location = IntField()
    cleanliness = IntField()
    check_in = IntField()
    service = IntField()
    business_service = IntField()


# definicso funzione che riceve un indice, i dati dell'hotel e tutte le review di quell hotel, crea
#  il documento e lo salva
def insert_data(index, name, place, rating, avg_prize, url, reviews):
    hotel = Hotel()
    hotel.index = index
    hotel.name = name
    hotel.place = place
    hotel.rating = rating
    hotel.avg_prize = avg_prize
    hotel.url = url
    hotel.reviews = reviews
    hotel.save()

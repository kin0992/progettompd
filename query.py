# coding=utf-8
from mongoengine import *
from math import log, e

from temp_db import Unique_bigrams, insert_unique_bigrams
from bigrams_saver import Review_bigrams


connect('progettoMPD')


# SELEZIONARE BIGRAMMI DI TUTTE LE RECENSIONI CON RATING N
def find_bigrams_with_sentiment(sentiment_val):
    list_bigrams = []
    bigrammi = Review_bigrams.objects(sentiment=sentiment_val)
    for bigramma in bigrammi:
        list_bigrams.append(bigramma.bigrams)
    list_bigrams = [y for x in list_bigrams for y in x]
    return list_bigrams


# SELEZIONARE BIGRAMMI DI TUTTE LE RECENSIONI CON RATING N E SAPERE QUANTI SONO
def count_bigram_with_sentiment(sentiment_val):
    list_bigrams = []
    bigrammi = Review_bigrams.objects(sentiment=sentiment_val)
    for bigramma in bigrammi:
        list_bigrams.append(bigramma.bigrams)
    list_bigrams = [y for x in list_bigrams for y in x]
    return len(list_bigrams)


# LISTA DI BIGRAMMI PRESENTI NELLA RECENSIONE
def find_bigrams_of_review(rev_index):
    list_bigrams = []
    bigrammi = Review_bigrams.objects(review_id=rev_index)
    for bigramma in bigrammi:
        list_bigrams.append(bigramma.bigrams)
    list_bigrams = [y for x in list_bigrams for y in x]
    return list_bigrams


# CONTA OCCORRENZE BIGRAMMA NELLA RECENSIONE
def count_bigram_in_review(bigram, rev_id):
    bigrams = Review_bigrams.objects(review_id=rev_id).scalar('bigrams')
    return bigrams[0].count(bigram)


# conta quante volte un bigramma è presente in tutte le recensioni
def count_bigram(bigram):
    frequency = Review_bigrams.objects(bigrams=bigram).count()
    return frequency


# Conta i bigrammi di una recensione
def count_bigrams_in_review(rev_id):
    return Review_bigrams.objects(review_id = rev_id).scalar('bigrams').count()


# CONTARE TUTTE LE REVIEW
def count_review():
    num_docs = Review_bigrams.objects().count()
    return num_docs


# calcola TF-IDF del bigramma ricevuto in input con l'id della recensione dove compare
def tf_idf(bigram, rev_id):
    n_bigram_in_review = count_bigram_in_review(bigram, rev_id)
    bigrams_in_review = count_bigrams_in_review(rev_id)
    tf = float(n_bigram_in_review)/bigrams_in_review

    n_review = count_review()
    frequency = count_bigram(bigram)
    idf = log(float(n_review)/frequency, e)

    return tf*idf


# CREA IN RAM L'INSIEME DI BIGRAMMI UNIVOCI DA INSERIRE NEL DB
def find_set_unique_bigrams():
    bigrams_set = set()
    bigrams_docs = Review_bigrams.objects
    for doc in bigrams_docs:
        for bigram in doc.bigrams:
            bigrams_set.add(tuple(bigram))
            # print bigram
    # inserimento nel db
    while not len(bigrams_set) == 0:
        print len(bigrams_set)
        bigram = bigrams_set.pop()
        rev_id = Review_bigrams.objects(bigrams=bigram).first().review_id
        # print rev_id
        frequency = count_bigram(bigram)
        tfidf = tf_idf(bigram, rev_id)
        # print frequency
        # print tfidf
        insert_unique_bigrams(bigram, frequency, tfidf)
    return bigrams_set


# restituisce una lista con tutti i bigrammi univoci trovati
def find_all_unique_bigrams():
    bigram_list = Unique_bigrams.objects.scalar('bigrams')
    return bigram_list


# CONTARE TUTTE LE REVIEW DI UN DATO SENTIMENT
def count_review_given_rating(sentiment):
    num_docs = Review_bigrams.objects(sentiment=sentiment).count()
    return num_docs


# dato un bigramma ed un sentiment trova il numero di recensioni con lo stesso sentiment che contengono il bigramma
def count_review_with_given_sentiment_with_given_bigram2(sentiment, bigram):
    reviews_number = Review_bigrams.objects(sentiment=sentiment, bigrams=bigram).count()
    return reviews_number


# ritorna una lista di bigrammi le cui frequenze sono comprese in un dato range di valori
def cut_bigrams_for_frequence(min, max):
    bigram_list = Unique_bigrams.objects(frequency__gt=min,  frequency__lt=max).scalar('bigrams')
    return bigram_list


# ritorna il numero di bigrammi compresi in un range di frequenze
def cut_bigrams_for_frequence2(min, max):
    bigram_list = Unique_bigrams.objects(frequency__gt=min,  frequency__lt=max).scalar('bigrams').count()
    return bigram_list

# ritorna una lista contenente i bigrammi ordinati in ordine decrescente secondo le frequenze
def a():
    bigrams = Unique_bigrams.objects.scalar('frequency').order_by('-frequency')
    print bigrams[0]


# print cut_bigrams_for_frequence2(8, 13141)
# print cut_bigrams_for_frequence2(120, 127)
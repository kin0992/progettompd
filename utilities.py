# coding=utf-8
import sys
from query import *


# data una lista di bigrammi crea la rete di bayes corrispondente e la salva nel formato JSON
def create_JSON(bigramsList):
    epsilon = sys.float_info.epsilon
    tot_bigrams = len(bigramsList)
    json = "{\"V\": [\"S\","
    vertex = ""
    edge = ""
    node = ""
    children = ""
    tot_rec_1 = 8142
    # count_review_given_rating(1)
    tot_rec_2 = 11243
    # count_review_given_rating(2)
    tot_rec_3 = 13632
    # count_review_given_rating(3)
    tot_rec_4 = 41605
    # count_review_given_rating(4)
    tot_rec_5 = 64308
    # count_review_given_rating(5)
    print "inizio for"
    for bigram in bigramsList:
        vertex = "".join([vertex, "\"", str(bigram[0]), " ", str(bigram[1]), "\", "])
        edge = "".join([edge, "[\"S\", \"", str(bigram[0]), " ", str(bigram[1]), "\"],"])
        prob1 = calcola_probs(bigram, 1, tot_rec_1)
        prob2 = calcola_probs(bigram, 2, tot_rec_2)
        prob3 = calcola_probs(bigram, 3, tot_rec_3)
        prob4 = calcola_probs(bigram, 4, tot_rec_4)
        prob5 = calcola_probs(bigram, 5, tot_rec_5)
        if prob1 == 0:
            prob1 = epsilon
        if prob2 == 0:
            prob2 = epsilon
        if prob3 == 0:
            prob3 = epsilon
        if prob4 == 0:
            prob4 = epsilon
        if prob5 == 0:
            prob5 = epsilon
        node = "".join([node, "\"", str(bigram[0]), " ", str(bigram[1]), "\": {\"ord\": ", str(tot_bigrams), ", \"numoutcomes\" : 2, "
                                        "\"vals\": [\"1\", \"0\"], "
                                        "\"parents\": [\"S\"], "
                                        "\"children\": None, "
                                        "\"cprob\": { "
                                        "\"['1']\": [", str('%.16f' % prob1)[1:], "," + str('%.16f' % (1-prob1))[1:], "],"
                                        "\"['2']\": [", str('%.16f' % prob2)[1:], "," + str('%.16f' % (1-prob2))[1:], "],"
                                        "\"['3']\": [", str('%.16f' % prob3)[1:], "," + str('%.16f' % (1-prob3))[1:], "],"
                                        "\"['4']\": [", str('%.16f' % prob4)[1:], "," + str('%.16f' % (1-prob4))[1:], "],"
                                        "\"['5']\": [", str('%.16f' % prob5)[1:], "," + str('%.16f' % (1-prob5))[1:], "]}},"])
        children = "".join([children, "\"", str(bigram[0]), " ", str(bigram[1]), "\", "])
        tot_bigrams -= 1
        print ((2284731-float(tot_bigrams))/float(2284731))*100
    vertex = vertex[:vertex.rfind(",")]
    edge = edge[:edge.rfind(",")]
    children = children[:children.rfind(",")]
    json = "".join([json, vertex])
    json = "".join([json, "], \"E\":["])
    json = "".join([json, edge])
    json = "".join([json, "], \"Vdata\": {"])
    json = "".join([json, node])
    json = "".join([json, "\"S\": {"
            "\"ord\": 0,"
            "\"numoutcomes\": 5,"
            "\"vals\": [\"1\", \"2\", \"3\", \"4\", \"5\"],"
            "\"parents\": None,"
            "\"children\": ["])
    json = "".join([json, children])
    json = "".join([json, "], \"cprob\": [.2, .2, .2, .2, .2]}}}"])
    out_file = open("500.txt", "w")
    out_file.write(json)
    out_file.close()


# calcola la probabilità che un bigramma compaia in una recensione con un dato sentiment
def calcola_probs(bigram, sentiment, den):
    bigram_in_review = count_review_with_given_sentiment_with_given_bigram2(sentiment, bigram)
    prob = float(bigram_in_review)/float(den)
    return prob

# parametri da decidere per il taglio dei bigrammi (estremi esclusi)
min = 120
max = 127
create_JSON(cut_bigrams_for_frequence(min, max))

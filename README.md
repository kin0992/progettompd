# PROGETTO MODELLI PROBABILISTICI

## DEPENDENCIES


1. mongoengine

  ```
  pip install mongoengine
  ```

2. langdetect

  ```
  pip install langdetect
  ```

3. stop-words

  ```
  pip install stop-words
  ```

4. stemming

  ```
  pip install stemming
  ```

5. nltk

  ```
  pip install nltk
  ```

    Per scaricare dei pacchetti utili per usare le funzioni:

    Aprire python da terminale e poi

    ```
    import nltk
    ```

    ```
    nltk.download()
    ```

    e poi scaricare i package dalla finestra che si apre.

6. libpgm

  ```
  pip install libpgm
  ```

## PER ESPORTARE IL DB

```
mongoexport --db progettoMPD --collection unique_bigrams --out bigrammi_univoci.json
```

## PER IMPORTARE IL DB:

```
mongoimport -d progettoMPD -c hotel -j 16 --file hotel.json
```


## QUERY 'COMPLESSE'

Per mostrare il CONTENT di tutte le recensioni scritte da 'Pinco,' presenti nel DB, il comando è:

```
db.hotel.find({"reviews.Author":"Pinco"}, {"reviews.Content":1}).pretty()
```

dove il primo argomento, {args} è ciò che vogliamo cercare (in questo caso tutte le recensioni di Pinco), mentre il secondo argomento è ciò che vogliamo mostrare; 1 per dire che si vuole visualizzare solo il Content di ogni review

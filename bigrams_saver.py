from mongoengine import *
from bigrams import init

connect('progettoMPD')

# classe per i bigrammi che verrano inseriti nel db
class Review_bigrams(Document):
    review_id = IntField()
    bigrams = ListField()
    sentiment = IntField()


# funzione che riceve in input il bigramma, l'id della recensione a cui appartiene e la valutazione,
# crea il documento bigramma e lo salva
def insert_bigrams(review_id, content, overall):
    review_bigrams = Review_bigrams()
    review_bigrams.review_id = review_id
    review_bigrams.sentiment = overall
    review_bigrams.bigrams = init(content)
    review_bigrams.save()

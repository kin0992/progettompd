from bayesianNetwork import *


connect('progettoMPD')

# chiamata alla funzione che carica la rete di Bayes in memoria
bn = load_network()
print "rete caricata"


# aspetta che vengano inserite nuove recensioni nel db e ne calocola il sentiment
while True:
    docs = Review_testing_run_time.objects(calcolato=False)
    for doc in docs:
        review_id = doc.review_id
        content = doc.content
        overall_algo = factorization_real_time(bn, content)
        print overall_algo
        Review_testing_run_time.objects(review_id=review_id).modify(overall_algo=overall_algo)
        Review_testing_run_time.objects(review_id=review_id).modify(calcolato=True)
